import { Component, OnInit } from '@angular/core';
import { AppState } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { reiniciar } from '../contador.actions';

@Component({
  selector: 'app-nieto',
  templateUrl: './nieto.component.html',
  styles: [
  ]
})
export class NietoComponent implements OnInit {
  contador: number;

  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.store.select('contador')
    .subscribe(contador => this.contador = contador);
  }

  reeset() {
    // this.contador = 0;
    // this.contadorCambio.emit(this.contador);
    this.store.dispatch(reiniciar({numero: 20}));
  }
}
